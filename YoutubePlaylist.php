<?php
require_once dirname(__FILE__).'/YoutubeHelper.php';

class YoutubePlaylist {

	CONST RANDOM_MODE_FULL_RANDOM = 'random_full'; //real random
	CONST RANDOM_MODE_ALTERNATING = 'random_alternate'; //randomize playlist items but loop playlists
	private $youtubeHelper;
	private $action;
	private $config;

	/*
	 @TODO refactoren und in services auslagern
	interface
	doppelte videos nicht in die playlist geben
	 */
	public function __construct() {
		$this->youtubeHelper = new YoutubeHelper();
		$this->action = (isset($_GET['action'])) ? $_GET['action'] : null;
		$this->config = json_decode(file_get_contents("config.json"));
	}

	public function handleRequest() {
		switch($this->action) {
			case 'delete':
				$this->youtubeHelper->deletePlaylistItemsFromPlaylist($this->config->importPlaylistId);
				break;
			case 'import':
				$playlists = $this->youtubeHelper->getInsertablePlaylists($this->getPlaylistIds());
				$videoIds = $this->getRandomizedVideoIdsFromPlaylists($playlists, YoutubePlaylist::RANDOM_MODE_ALTERNATING);
				$this->youtubeHelper->addPlaylistItemsToPlaylistByVideoIds($videoIds, $this->config->importPlaylistId);
				break;
		}

		require("overview.phtml");
	}

	private function outputStats() {
		foreach ($this->youtubeHelper->getStats() as $name => $stat) {
			echo $name.': '.$stat.'<br />';
		}
	}

	private function getRandomizedVideoIdsFromPlaylists($playlists, $randomMode) {
		$videoIds = array();
		if ($randomMode == YoutubePlaylist::RANDOM_MODE_ALTERNATING) {
			foreach ($playlists as $key => $videoIdsFromPlaylist) {
				$playlists[$key] = $this->shuffleAndSlicePlaylist($videoIdsFromPlaylist);
			}
			for ($i = 0; $i < 50; $i++) {
				foreach ($playlists as $videoIdsFromPlaylist) {
					if (array_key_exists($i, $videoIdsFromPlaylist)) {
						$videoIds[] = $videoIdsFromPlaylist[$i];
					}
				}
			}
		} else if ($randomMode == YoutubePlaylist::RANDOM_MODE_FULL_RANDOM) {
			foreach ($playlists as $videoIdsFromPlaylist) {
				$videoIds = array_merge($videoIds, $this->shuffleAndSlicePlaylist($videoIdsFromPlaylist));
			}
			shuffle($videoIds);
		}

		return $videoIds;
	}

	private function shuffleAndSlicePlaylist($playlist) {
		shuffle($playlist);

		return array_slice($playlist, 0, $this->config->maxItemsFromSinglePlaylist);
	}

	private function getPlaylistIds() {
		$ids = [];
		foreach($this->config->playlists as $playlist) {
			$ids[] = $playlist->id;
		}

		return $ids;
	}
}