<?php
session_start();
ini_set('display_errors', 1);
$config = json_decode(file_get_contents("config.json"));
try {
	require_once __DIR__.'/vendor/autoload.php';
	require_once dirname(__FILE__).'/YoutubePlaylist.php';

	if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
		$ytPlaylist = new YoutubePlaylist();
		$ytPlaylist->handleRequest();
	} else {
		$redirect_uri = $config->baseUrl.'auth/';
		header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
	}
}catch (Exception $e) {
	$msg = json_decode($e->getMessage());
	if($msg && $msg->error->message == 'Invalid Credentials') {
		unset($_SESSION['access_token']);
		$redirect_uri = $config->baseUrl;
		header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
	}
	echo $e->getMessage();
}
